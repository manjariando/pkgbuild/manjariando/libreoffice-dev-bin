# Maintainer: twa022 <twa022 at gmail dot com>

_pkgname=libreoffice
pkgname=${_pkgname}-dev-bin
_pkgnamefmt=LibreOffice
_LOver=7.2.3.2
pkgver=$(cut -f1-4 -d'.' <<< ${_LOver})
_basever=$( cut -f1-2 -d'.' <<< ${_LOver})
pkgrel=1
arch=('x86_64')
license=('LGPL3')
url="https://www.libreoffice.org"
pkgdesc="LibreOffice development branch"
provides=("${_pkgname}=${pkgver}" "${_pkgname}-en-US=${pkgver}" "${_pkgname}-dev=${pkgver}")
options=('!strip')

source_x86_64=("https://dev-builds.libreoffice.org/pre-releases/rpm/x86_64/${_pkgnamefmt}_${_LOver}_Linux_x86-64_rpm.tar.gz"
            "https://metainfo.manjariando.com.br/${_pkgname}/${_pkgname}"-{base,calc,draw,impress,math,startcenter,writer}.appdata.xml
            "https://metainfo.manjariando.com.br/${_pkgname}/org.libreoffice.kde.metainfo.xml")
sha256sums_x86_64=('0a92fba6d2a74fb811ae9065d9b46457c681f3d5fb9e85ff05fece849bbfd5eb'
                   '51f2763eb2e113a5d216f98aa844cdd960347b1a88f2a68e4fee1688a2c69690'
                   '0a2b5a4ec69b991d579fd13840277a1f36cffd3bb59eb653878ab98f480dcc09'
                   '0bb636a9dcf986d275afeac5e0b0390521a82a701ded5651e6e3f36055e8eb96'
                   '75b73643a745f27ccc027ed33dacc941a5f1751473086c36b28fa16dbff7edd4'
                   '548e266c9bb8fd79ec4cf8584ea5ed3908650e3ff545eba3d5d996e64b572d4a'
                   'f1bb46e5f1aae2eb0fce40dd7d350b4cf12b3ac28f4e0d1e1e0784f54fe9592d'
                   'f6f675adc8b5d4825fcc1eac099ccf894e37b2ef8a88f4ba7b6c256a647644cd'
                   '29079f14216b9d108026fc426514999b05a830cc9f723bf243cb61f3d44bf959')

package() {
    depends=('gtk3' 'lpsolve' 'neon' 'curl')
    optdepends=('java-runtime:          adds java support'
                'java-environment:      required by extension-wiki-publisher and extension-nlpsolver'
                'coin-or-mp:            required by the Calc solver'
                'kio:                   for Qt5 integration')

    find "${srcdir}/${_pkgnamefmt}_${_LOver}"*/RPMS/*rpm -exec bsdtar -x -f '{}' -C "${pkgdir}" \;

    # Appstream
    rm -rf "${pkgdir}"/usr/share/metainfo/*
    install -D -m644 "${srcdir}/${_pkgname}-base.appdata.xml" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-base.appdata.xml"
    install -D -m644 "${srcdir}/${_pkgname}-calc.appdata.xml" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-calc.appdata.xml"
    install -D -m644 "${srcdir}/${_pkgname}-draw.appdata.xml" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-draw.appdata.xml"
    install -D -m644 "${srcdir}/${_pkgname}-impress.appdata.xml" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-impress.appdata.xml"
    install -D -m644 "${srcdir}/${_pkgname}-math.appdata.xml" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-math.appdata.xml"
    install -D -m644 "${srcdir}/${_pkgname}-startcenter.appdata.xml" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-startcenter.appdata.xml"
    install -D -m644 "${srcdir}/${_pkgname}-writer.appdata.xml" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-writer.appdata.xml"
    install -D -m644 "${srcdir}/org.${_pkgname}.kde.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.${_pkgname}${_basever}.kde.metainfo.xml"
    
    #
    sed -i "/icon type/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-base.appdata.xml"
    sed -i "/icon type/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-calc.appdata.xml"
    sed -i "/icon type/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-draw.appdata.xml"
    sed -i "/icon type/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-impress.appdata.xml"
    sed -i "/icon type/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-math.appdata.xml"
    sed -i "/icon type/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-startcenter.appdata.xml"
    sed -i "/icon type/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-writer.appdata.xml"

    #
    sed -i "/id/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-base.appdata.xml"
    sed -i "/id/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-calc.appdata.xml"
    sed -i "/id/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-draw.appdata.xml"
    sed -i "/id/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-impress.appdata.xml"
    sed -i "/id/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-math.appdata.xml"
    sed -i "/id/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-startcenter.appdata.xml"
    sed -i "/id/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/${_pkgname}${_basever}-writer.appdata.xml"
    sed -i "/id/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/org.${_pkgname}${_basever}.kde.metainfo.xml"

    #
    sed -i "/extends/ s/libreoffice/libreoffice${_basever}/" "${pkgdir}/usr/share/metainfo/org.${_pkgname}${_basever}.kde.metainfo.xml"
}
